<?php

declare(strict_types=1);

namespace Grifix\TypesBundle\Normalizers;

use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\Exceptions\InvalidObjectTypeException;
use Grifix\Uuid\Uuid;

final class UuidNormalizer implements CustomObjectNormalizerInterface
{

    public function normalize(object $object): array
    {
        if ( ! ($object instanceof Uuid)) {
            throw new InvalidObjectTypeException($object::class, Uuid::class);
        }

        return ['value' => $object->toString()];
    }

    public function denormalize(array $data): object
    {
        return Uuid::createFromString($data['value']);
    }

    public function getObjectClass(): string
    {
        return Uuid::class;
    }
}
