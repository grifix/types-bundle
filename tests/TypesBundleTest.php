<?php

declare(strict_types=1);

namespace Grifix\TypesBundle\Tests;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Ip\IpAddress;
use Grifix\Money\Currency\Currency;
use Grifix\Money\Money\Money;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\NormalizerBundle\GrifixNormalizerBundle;
use Grifix\TypesBundle\GrifixTypesBundle;
use Grifix\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class TypesBundleTest extends KernelTestCase
{

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var Kernel $kernel
         */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixTypesBundle::class);
        $kernel->addTestBundle(GrifixNormalizerBundle::class);
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();

        /** @var NormalizerInterface $normalizer */
        $normalizer = $kernel->getContainer()->get(NormalizerInterface::class);

        $uuid = Uuid::createFromString('2db5e8e4-06f9-418f-8df6-f6d9736879bf');
        self::assertEquals($uuid, $normalizer->denormalize($normalizer->normalize($uuid)));

        $money = Money::usd(100);
        self::assertEquals($money, $normalizer->denormalize($normalizer->normalize($money)));

        $dateTimeImmutable = new \DateTimeImmutable('2001-01-01');
        self::assertEquals($dateTimeImmutable, $normalizer->denormalize($normalizer->normalize($dateTimeImmutable)));

        $dateTime = new \DateTimeImmutable('2001-01-01');
        self::assertEquals($dateTime, $normalizer->denormalize($normalizer->normalize($dateTime)));

        $currency = Currency::usd();
        self::assertEquals($currency, $normalizer->denormalize($normalizer->normalize($currency)));

        $dateTime = DateTime::create(2001, 1, 1);
        self::assertEquals($dateTime, $normalizer->denormalize($normalizer->normalize($dateTime)));

        $ip = Email::create('user@example.com');
        self::assertEquals($ip, $normalizer->denormalize($normalizer->normalize($ip)));

        $ip = IpAddress::create('127.0.0.1');
        self::assertEquals($ip, $normalizer->denormalize($normalizer->normalize($ip)));
    }
}
