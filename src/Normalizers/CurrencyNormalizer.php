<?php

declare(strict_types=1);

namespace Grifix\TypesBundle\Normalizers;

use Grifix\Money\Currency\Currency;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\Exceptions\InvalidObjectTypeException;

final class CurrencyNormalizer implements CustomObjectNormalizerInterface
{

    public function normalize(object $object): array
    {
        if ( ! ($object instanceof Currency)) {
            throw new InvalidObjectTypeException($object::class, Currency::class);
        }

        return ['value' => $object->getCode()];
    }

    public function denormalize(array $data): object
    {
        return Currency::withCode($data['value']);
    }

    public function getObjectClass(): string
    {
        return Currency::class;
    }
}
