<?php

declare(strict_types=1);

namespace Grifix\TypesBundle\DependencyInjection;

use Grifix\NormalizerBundle\GrifixNormalizerBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class GrifixTypesExtension extends Extension implements PrependExtensionInterface
{

    public function prepend(ContainerBuilder $container)
    {
        $this->checkBundles($container);
        $this->loadConfigs($container);
    }

    private function checkBundles(ContainerBuilder $container): void
    {
        $bundles = $container->getParameter('kernel.bundles');
        $this->assertBundleEnabled(FrameworkBundle::class, $bundles);
        $this->assertBundleEnabled(GrifixNormalizerBundle::class, $bundles);
    }

    private function assertBundleEnabled(string $bundle, array $bundles)
    {
        if ( ! in_array($bundle, $bundles)) {
            throw new RuntimeException(sprintf('%s bundle must be enabled!', $bundle));
        }
    }

    public function load(array $configs, ContainerBuilder $container)
    {

    }

    private function loadConfigs(ContainerBuilder $container): void
    {
        $configDir = __DIR__ . '/../..';
        $loader    = new YamlFileLoader($container, new FileLocator($configDir));
        $loader->load('config.yaml');
    }
}
