<?php

declare(strict_types=1);

namespace Grifix\TypesBundle\Normalizers;

use Grifix\Money\Money\Money;
use Grifix\Normalizer\ObjectNormalizers\CustomObjectNormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\Exceptions\InvalidObjectTypeException;

final class MoneyNormalizer implements CustomObjectNormalizerInterface
{

    public function normalize(object $object): array
    {
        if ( ! ($object instanceof Money)) {
            throw new InvalidObjectTypeException($object::class, Money::class);
        }

        return [
            'amount'   => $object->getAmount(),
            'currency' => $object->getCurrency()->getCode()
        ];
    }

    public function denormalize(array $data): object
    {
        return Money::withCurrency($data['amount'], $data['currency']);
    }

    public function getObjectClass(): string
    {
        return Money::class;
    }
}
